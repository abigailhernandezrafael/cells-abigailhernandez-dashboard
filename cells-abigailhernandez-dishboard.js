{
  const {
    html,
  } = Polymer;
  /**
    `<cells-abigailhernandez-dishboard>` Description.

    Example:

    ```html
    <cells-abigailhernandez-dishboard></cells-abigailhernandez-dishboard>
    ```

    ## Styling
    The following custom properties and mixins are available for styling:

    ### Custom Properties
    | Custom Property     | Selector | CSS Property | Value       |
    | ------------------- | -------- | ------------ | ----------- |
    | --cells-fontDefault | :host    | font-family  |  sans-serif |
    ### @apply
    | Mixins    | Selector | Value |
    | --------- | -------- | ----- |
    | --cells-abigailhernandez-dishboard | :host    | {} |

    * @customElement
    * @polymer
    * @extends {Polymer.Element}
    * @demo demo/index.html
  */
  class CellsAbigailhernandezDishboard extends Polymer.Element {

    static get is() {
      return 'cells-abigailhernandez-dishboard';
    }

    static get properties() {
      return {
        condicional:{
          type: Boolean,
          value: false
        },
        lista:{
          type: Object,
          notify: true,
          value: {}
        },
        uss:{
          type: String,
          value: ''
        }
      };
    }

    filtro(item){
      //return item.name.toLowerCase()==this.uss.toLowerCase();
    }

    refiltrer(item){
      //recargar dom para pintar lo que se crea
      Polymer.dom(this.root).querySelector("#uss").render();
    }

    static get template() {
      return html `
      <style include="cells-abigailhernandez-dishboard-styles cells-abigailhernandez-dishboard-shared-styles"></style>
      <slot></slot>

      <template is="dom-if" if="{{!condicional}}">
      <component-login prop4={{condicional}}></component-login>
    </template>
    <cells-mocks-component alumnos={{cards}}></cells-mocks-component>
        
    <!-- Mostrar componentes de JSON-->
        <div>[[handleResponse.value]]</div>
        <p>Filtro <input type="text" value="{{uss::input}}" on-keyup="refiltrer"></p>
      <cells-mocks-component alumnos="{{lista}}"></cells-mocks-component>
        <template is="dom-repeat" items="{{lista}}" filter="{{filtro(uss)}}" id="users">
            <div class="card">
                <img src="{{item.img}}" alt="No encuentra la Imagen .png">
                <div>Firts Name: <span>{{item.name}}</span></div>
                <div>Last Name: <span>{{item.last}}</span></div>
                <div>Last Address: <span>{{item.address}}</span></div>
                <div>Last Hobbies: <span>{{item.hobbies}}</span></div>
              </div>
        </template>
      `;
    }
  }

  customElements.define(CellsAbigailhernandezDishboard.is, CellsAbigailhernandezDishboard);
}